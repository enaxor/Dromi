/*
 * Copyright (C) 2016 @xarone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.dromi;

import android.content.Context;

/**
 * Created by @xarone on 8/17/16.
 * Simple class that hold constants variables
 */
public class Constants {

    static final boolean DEBUG = false;
    // Broadcast strings
    static final String LOCAL_BROADCAST_UPDATE_UI = "update_ui_tools_event";
    static final String LOCAL_BROADCAST_UNDO_KEY = "undo";
    static final String LOCAL_BROADCAST_REDO_KEY = "redo";
    static String applicationName;

    public static String getApplicationName(Context context) {
        if (applicationName == null) {
            applicationName = context.getString(context.getApplicationInfo().labelRes);
        }
        return applicationName;
    }
}
