/*
 * Copyright (C) 2016 @xarone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.dromi;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;

/**
 * Created by @xarone on 8/14/16.
 * Filesystem tools
 */
public class Tools {

    /**
     * Checks if external storage is available for read and write
     *
     * @return true if writable, false otherwise
     */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /**
     * Generate a unique filename with date (typically drawing_20167172148.png)
     *
     * @return the new filename as a string
     */
    public static String getFileName() {
        Calendar c = Calendar.getInstance();
        return "drawing_" + c.get(Calendar.YEAR) + c.get(Calendar.MONTH) +
                c.get(Calendar.DAY_OF_MONTH) + c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE) + ".png";
    }

    /**
     * Get a custom picture folder
     *
     * @param directoryName directory name where to store pics
     * @return a ready to use picture directory
     */
    public static File getPicturesFolder(String directoryName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "/" + directoryName + "/");
        file.mkdirs();
        return file;
    }

    // the lazy toast
    public static void notifyUser(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }
}
