/*
 * Copyright (C) 2016 @xarone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.dromi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by @xarone on 8/9/16.
 * View that hold the 2d canvas containing user's drawing paths
 */

public class CanvasView extends View {

    public static final String TAG = "CanvasView";
    // Because fingers are big and imprecise
    private static final float FINGER_TOLERANCE = 5;
    // Host the draw calls
    private Canvas mCanvas;
    // Store pixels
    private Bitmap mBitmap;
    // Store segments and curves
    private Path mPath;
    // Store colors and styles
    private Paint mPaint;
    // Store current coordinates
    private float mX, mY;
    // Stores brushstroke operations
    private ArrayList<Brushstroke> mUndoHistory;
    private ArrayList<Brushstroke> mRedoHistory;

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mUndoHistory = new ArrayList<Brushstroke>();
        mRedoHistory = new ArrayList<Brushstroke>();
        // Create and config a new Paint with default brush color and size
        initPaint(Color.BLACK, 4f);
        // Create a new Path
        mPath = new Path();
    }

    /* Drawing actions */

    /**
     * User start drawing on the canvas according to the x and y values
     *
     * @param x The x coordinate of the touch
     * @param y The y coordinate of the touch
     */
    private void startTouch(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    /**
     * User stopped drawing, draw path on the canvas
     */
    private void upTouch() {
        if ((int) new PathMeasure(mPath, false).getLength() == 0.0) {
            // Path is empty
            if (Constants.DEBUG) Log.d(TAG, "Errand touch ignored.");
            return;
        }
        mRedoHistory.clear();
        mPath.lineTo(mX, mY);
        mUndoHistory.add(new Brushstroke(mPath, mPaint));
        mPath = new Path(); // Reset path
    }

    /**
     * User is drawing on the canvas according to the x and y values
     *
     * @param x The x coordinate of the move
     * @param y The y coordinate of the move
     */
    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= FINGER_TOLERANCE || dy >= FINGER_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    /**
     * Send a local broadcast event to update UI
     */
    private void updateUi() {
        Intent intent = new Intent(Constants.LOCAL_BROADCAST_UPDATE_UI);
        // Toggle undo/redo button
        intent.putExtra(Constants.LOCAL_BROADCAST_UNDO_KEY, mUndoHistory.size() != 0);
        intent.putExtra(Constants.LOCAL_BROADCAST_REDO_KEY, mRedoHistory.size() != 0);
        // Send broadcast
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    /**
     * Create a configure a paint with new configuration
     *
     * @param color new brush color
     * @param size  new brush size
     */
    private void initPaint(int color, float size) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(size);
        mPaint.setColor(color);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    /* Public methods */

    /**
     * Reset the drawing area
     */
    public void clearCanvas() {
        mUndoHistory.clear();
        mRedoHistory.clear();
        mPath.reset();
        mBitmap.eraseColor(Color.WHITE);
        invalidate();
    }

    /**
     * Undo the last brushstroke
     */
    public void undo() {
        if (mUndoHistory.size() > 0) {
            Brushstroke brushstrokeToDelete = mUndoHistory.get(mUndoHistory.size() - 1);
            mUndoHistory.remove(brushstrokeToDelete);
            mRedoHistory.add(brushstrokeToDelete);
            invalidate();
            if (Constants.DEBUG)
                Log.d(TAG, "Removed brushstroke: " + brushstrokeToDelete.toString());
        } else {
            if (Constants.DEBUG)
                Log.d(TAG, "Undo history empty.");
        }
    }

    /**
     * Redo the next brushstroke
     */
    public void redo() {
        if (mRedoHistory.size() > 0) {
            Brushstroke brushstrokeToAdd = mRedoHistory.get(mRedoHistory.size() - 1);
            mUndoHistory.add(brushstrokeToAdd);
            mRedoHistory.remove(brushstrokeToAdd);
            invalidate();
            if (Constants.DEBUG)
                Log.d(TAG, "Added brushstroke: " + brushstrokeToAdd.toString());
        } else {
            if (Constants.DEBUG)
                Log.d(TAG, "Redo history empty.");
        }
    }

    public boolean isEmpty() {
        return mUndoHistory.isEmpty();
    }

    /* Override methods */

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // Write past paths on canvas
        for (Brushstroke brushStroke : mUndoHistory) {
            canvas.drawPath(brushStroke.getPath(), brushStroke.getPaint());
        }
        // Draw current path on canvas
        canvas.drawPath(mPath, mPaint);
        // Update ui
        updateUi();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate(); // Refresh view
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Init bitmap and canvas
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        // Erase bitmap to white
        mBitmap.eraseColor(Color.WHITE);
    }

    /* Getters and Setters */

    public int getCurrentColor() {
        return mPaint.getColor();
    }

    public void setCurrentColor(int color) {
        initPaint(color, getBrushSize());
    }

    public float getBrushSize() {
        return mPaint.getStrokeWidth();
    }

    public void setBrushSize(float size) {
        initPaint(getCurrentColor(), size);
    }

    public Bitmap getBitmap() {
        // Erase bitmap to white
        mBitmap.eraseColor(Color.WHITE);
        // Redraw bitmap
        for (Brushstroke bs : mUndoHistory) {
            mCanvas.drawPath(bs.getPath(), bs.getPaint());
        }
        return mBitmap;
    }
}
