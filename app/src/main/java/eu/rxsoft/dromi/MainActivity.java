/*
 * Copyright (C) 2016 @xarone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.dromi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by @xarone on 8/9/16.
 * MainActivity
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    // Edit the canvas from here
    private CanvasView mCanvasView;
    // Path of the saved drawing
    private String mPath = null;
    // Hold the seekBar temporary state
    private int mBrushSize = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCanvasView = (CanvasView) findViewById(R.id.main_canvas_view);
    }

    @Override
    protected void onResume() {
        // Register an observer to receive Intents with actions named Constants.LOCAL_BROADCAST_UPDATE_UI.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(Constants.LOCAL_BROADCAST_UPDATE_UI));
        super.onResume();
    }

    @Override
    protected void onPause() {
        // Unregister observer since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    /* Buttons action methods */

    /**
     * Show a custom action menu
     *
     * @param view menu called from this view
     */
    public void showPopupMenu(View view) {
        final Context context = this;
        PopupMenu popup = new PopupMenu(this, view);
        popup.inflate(R.menu.menu_main);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_save:
                        saveImageToExternalStorage();
                        return true;
                    case R.id.action_share:
                        shareDrawing();
                        return true;
                    case R.id.action_about:
                        final AppCompatDialog dialog = new AppCompatDialog(context);
                        dialog.setContentView(R.layout.dialog_about);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                        return true;
                    default: // User's action was not recognized
                        return false;
                }
            }
        });
        popup.show();
    }

    /**
     * Reset the canvas and erase all unsaved drawing
     *
     * @param view dialog called from this view
     */
    public void clearCanvas(View view) {
        if (mCanvasView.isEmpty()) return; // Reset is not needed
        // Prompt the user before clearing canvas
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.clear_canvas_prompt_title))
                .setMessage(getString(R.string.clear_canvas_prompt_text))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mCanvasView.clearCanvas();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Save the bitmap as a png to external storage
     *
     * @return false if not saved, true otherwise
     */
    private boolean saveImageToExternalStorage() {
        // check if external storage is writable
        if (!Tools.isExternalStorageWritable()) {
            Log.e(TAG, "App can't access to external storage: check permissions.");
            Tools.notifyUser(this, getString(R.string.save_error));
            return false;
        }
        // create file
        File folder = Tools.getPicturesFolder(Constants.getApplicationName(this));
        String fileName = Tools.getFileName();
        final File file = new File(folder, fileName);
        // store path
        mPath = file.getAbsolutePath();
        if (Constants.DEBUG) Log.d(TAG, fileName + " will be stored at " + mPath + ".");
        // Delete a previous version
        if (file.exists()) {
            if (Constants.DEBUG)
                Log.d(TAG, "A previous version at '" + mPath + "' exists. Overriding...");
            file.delete();
        }
        // Get Bitmap
        final Bitmap finalBitmap = mCanvasView.getBitmap();
        // Copy bitmap data to file
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Copy from bitmap to " + mPath + " failed: file is not existing anymore.");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, "Copy from bitmap to " + mPath + " failed: disk I/O error.");
            e.printStackTrace();
        } finally { // All went fine
            // Scan for new content so the file is immediately available
            MediaScannerConnection.scanFile(
                    getApplicationContext(),
                    new String[]{file.getAbsolutePath()},
                    null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            if (Constants.DEBUG)
                                Log.d(TAG, "Scan Completed for file " + path);
                        }
                    });
            Tools.notifyUser(getApplicationContext(), getString(R.string.save_confirm) + mPath);
        }
        return true;
    }

    /**
     * Share the drawing as an image
     */
    private void shareDrawing() {
        if (mPath == null) { // Save the drawing before sharing it
            if (!saveImageToExternalStorage()) {
                // Something went wrong when saving bitmap, abort sharing process
                return;
            }
        }
        File drawing = new File(mPath);
        // Create intent
        final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        // Config intent
        shareIntent.setType("image/png");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(drawing));
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.Intent_extra_subject) + Constants.getApplicationName(this) + "!");
        // Start intent
        startActivity(Intent.createChooser(shareIntent, getString(R.string.intent_chooser_title)));
    }

    /**
     * Set or unset undo Button
     *
     * @param toggle undo button status
     */
    private void toggleUndoButton(final boolean toggle) {
        if (toggle && findViewById(R.id.undoPaint).getAlpha() != 1) {
            setOpacityById(R.id.undoPaint, (float) 1);
            if (Constants.DEBUG) Log.d(TAG, "Enabled Undo Button");
        } else if (!toggle && findViewById(R.id.undoPaint).getAlpha() != 0.5) {
            setOpacityById(R.id.undoPaint, (float) 0.5);
            if (Constants.DEBUG) Log.d(TAG, "Disabled Undo Button");
        }
    }

    /**
     * Set or unset redo Button
     *
     * @param toggle redo button status
     */
    private void toggleRedoButton(final boolean toggle) {
        if (toggle && findViewById(R.id.redoPaint).getAlpha() != 1) {
            setOpacityById(R.id.redoPaint, (float) 1);
            if (Constants.DEBUG) Log.d(TAG, "Enabled Redo Button");
        } else if (!toggle && findViewById(R.id.redoPaint).getAlpha() != 0.5) {
            setOpacityById(R.id.redoPaint, (float) 0.5);
            if (Constants.DEBUG) Log.d(TAG, "Disabled Redo Button");
        }
    }

    /**
     * Set opacity a the given view
     *
     * @param id      id of the view to modify
     * @param opacity float between 0 and 1, is the opacity to set
     */
    private void setOpacityById(final int id, final float opacity) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(id).setAlpha(opacity);

            }
        });
    }

    /**
     * Use the QuadFlask:colorpicker for color selection
     *
     * @param view dialog called from this view
     * @link github.com/QuadFlask/colorpicker
     */
    public void colorPick(View view) {
        ColorPickerDialogBuilder
                .with(this)
                .setTitle(getString(R.string.color_picker_title))
                .initialColor(mCanvasView.getCurrentColor())
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(14)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                    }
                })
                .setPositiveButton("Ok", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        mCanvasView.setCurrentColor(selectedColor);
                        Tools.notifyUser(getApplicationContext(), String.format("#%06X", 0xFFFFFF & selectedColor));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    /**
     * Call a dialog with a SeekBar to change the brush size
     *
     * @param view dialog called from this view
     */
    public void brushSizePicker(View view) {
        // SeekBar configuration
        final SeekBar seek = new SeekBar(this);
        seek.setMax(100);
        seek.setProgress((int) mCanvasView.getBrushSize());
        seek.setKeyProgressIncrement(1);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mBrushSize = seekBar.getProgress();
            }
        });
        // Dialog configuration
        AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        popDialog.setIcon(R.drawable.ic_brush_picker);
        popDialog.setTitle(getString(R.string.brush_size_title));
        popDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mBrushSize < 0) {
                    Log.wtf(TAG, "Can't set brush size: seekBar did not set value.");
                    mBrushSize = 4;
                    return;
                }
                mCanvasView.setBrushSize(mBrushSize);
            }
        });
        popDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        // Add seekBar to dialog
        popDialog.setView(seek, 50, 70, 50, 15);
        // Show dialog
        popDialog.show();
    }

    /**
     * Undo the last brush stroke
     *
     * @param view dialog called from this view
     */
    public void undoPaint(View view) {
        mCanvasView.undo();
    }

    /**
     * Redo the next brush stroke
     *
     * @param view dialog called from this view
     */
    public void redoPaint(View view) {
        mCanvasView.redo();
    }

    /**
     * Called whenever an Intent with an action named Constants.LOCAL_BROADCAST_UPDATE_UI is broadcasted.
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Update undo & redo buttons
            toggleUndoButton(intent.getBooleanExtra(Constants.LOCAL_BROADCAST_UNDO_KEY, false));
            toggleRedoButton(intent.getBooleanExtra(Constants.LOCAL_BROADCAST_REDO_KEY, false));
        }
    };
}
