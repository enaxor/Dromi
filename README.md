# Dromi
Dromi (Draw me) is a finger painting app for Android devices.

## Issues and features
[The issues page](https://github.com/xarone/Dromi/issues) is for
reporting bugs and feature requests.

## License
* Dromi is licensed under the GPL and will always be free.
* [License of the used library](https://github.com/QuadFlask/colorpicker)

![screenshot.png](art/legateau.png)
